﻿-- DROP DATABASE IF EXISTS ejemplo3u3;
-- CREATE DATABASE IF NOT EXISTS ejemplo3u3;
USE ejemplo3u3;

/* Ej1 - Importar los datos del excel a mysql*/

/* Ej2 - Función área triangulo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej2(base int, altura int)
    RETURNS float
    COMMENT 'Función área triangulo'
  BEGIN
      -- Area de un triangulo
      DECLARE area_T float DEFAULT 0.0;
         
      SET area_T = base * altura / 2;

    RETURN area_T;
  END //
DELIMITER ;

SELECT ej2(10,23);

/* Ej3 - Función perímetro triangulo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej3(base int,lado2 int,lado3 int)
    RETURNS int
    COMMENT 'Función perímetro triangulo'
  BEGIN
    DECLARE perimetro float DEFAULT 0.0;
    SET perimetro = base + lado2 + lado3;
    RETURN perimetro;
  END //
DELIMITER ;

SELECT ej3(10,20,30);

/* Ej4 - Procedimiento almacenado triángulos */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4(id1 int ,id2 int)
    COMMENT 'Procedimiento almacenado triángulos'
    BEGIN
      UPDATE triangulos t
        SET t.area=ej2(t.base,t.altura),
            t.perimetro=ej3(t.base,t.lado2,t.lado3)
        WHERE t.id BETWEEN IF(id1<id2,id1,id2) AND IF(id2>id1,id2,id1);
    END //
DELIMITER ;

CALL ej4(10,20);

SELECT * FROM triangulos t;

/* Ej5 - Función área cuadrado */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej5(lado int)
    RETURNS int
    COMMENT 'Función área cuadrado'
  BEGIN
    DECLARE area_c int DEFAULT 0;
    SET area_c= POW(lado,2);
    RETURN area_c;
  END //
DELIMITER ;

SELECT c.id,ej5(c.lado) FROM cuadrados c;

/* Ej6 - Función perímetro cuadrado */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej6(lado int)   
    RETURNS int
    COMMENT 'Función perímetro cuadrado'
  BEGIN  
    DECLARE perimetro int DEFAULT 0;   
    SET perimetro = lado*4;
    RETURN perimetro;
  END //
DELIMITER ;

SELECT id,ej6(c.lado)FROM cuadrados c;


/* Ej7 - Procedimiento almacenado cuadrados */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7(id1 int, id2 int)
    COMMENT 'Procedimiento almacenado cuadrados'
    BEGIN
      UPDATE cuadrados c
        SET c.area=ej5(c.lado),
            c.perimetro=ej6(c.lado)
        WHERE c.id BETWEEN IF(id1<id2,id1,id2) AND  IF(id2>id1,id2,id1);        
    END //
DELIMITER ;

CALL ej7(10,20);

SELECT * FROM cuadrados;

/* Ej8 - Función área rectángulo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej8(lado1 int, lado2 int)
    RETURNS int
    COMMENT 'Función área rectángulo'
  BEGIN
    DECLARE area_r int DEFAULT 0;

    SET area_r = lado1 * lado2;

    RETURN area_r;
  END //
DELIMITER ;

SELECT id, r.lado1, r.lado2, ej8(lado1,lado2) areaRectangulo FROM rectangulo r;

/* Ej9 - Función perímetro rectángulo  */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej9(lado1 int, lado2 int)
    RETURNS int
    COMMENT 'Función perímetro rectángulo'
  BEGIN
    
    DECLARE perimetro int DEFAULT 0;
    SET perimetro = 2 * (lado1 + lado2);
    
    RETURN perimetro;
  END //
DELIMITER ;

SELECT id, r.lado1, r.lado2, ej9(lado1,lado2) perimetroRectangulo FROM rectangulo r;

/* Ej10 - Procedimiento almacenado rectángulo  */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej10(id1 int, id2 int)
    COMMENT 'Procedimiento almacenado rectángulo'
    BEGIN
      UPDATE rectangulo r
        SET r.area=ej8(r.lado1,r.lado2),
            r.perimetro=ej9(r.lado1,r.lado2)
        WHERE r.id BETWEEN IF(id1<id2,id1,id2) AND  IF(id2>id1,id2,id1);        
    END //
DELIMITER ;

CALL ej10(10,20);

SELECT * FROM rectangulo r;

/* Ej11 - Función área circulo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej11(radio int)
    RETURNS float
    COMMENT 'Función área circulo'
  BEGIN
    DECLARE area_c float DEFAULT 0;
    SET area_c = PI() * POW(radio,2);

    RETURN area_c;
  END //
DELIMITER ;

SELECT c.id, c.radio, ej11(c.radio) FROM circulo c;

/* Ej12 - Función área circulo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej12(radio int)
    RETURNS float
    COMMENT 'Función perímetro circulo'
  BEGIN
    DECLARE area_c float DEFAULT 0;
    SET area_c = 2 * PI() * radio;

    RETURN area_c;
  END //
DELIMITER ;

SELECT c.id, c.radio, ej12(c.radio) FROM circulo c;

/* Ej13 - Procedimiento almacenado círculos */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej13(id1 int,id2 int, tipo char(1))
    COMMENT 'Procedimiento almacenado círculos'
    BEGIN
      UPDATE circulo c
        SET c.area = ej11(c.radio),
            c.perimetro=ej12(c.radio)            
          WHERE 
            c.id BETWEEN IF(id1<id2,id1,id2) AND  IF(id2>id1,id2,id1)
          AND
            c.tipo=tipo; -- mejorar para null
    END //
DELIMITER ;

CALL ej13(10,20,'a');

SELECT * FROM circulo c;

/* Ej14 - Función media */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej14(nota1 int, nota2 int, nota3 int, nota4 int)
    RETURNS float
    COMMENT 'Función media'
  BEGIN
    DECLARE resultado float DEFAULT 0;

    SET resultado = (nota1 + nota2 + nota3 + nota4) / 4; 

    RETURN resultado;
  END //
DELIMITER ;

SELECT ej14(20,41,50,60);

/* Ej15 - Función mínimo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej15(nota1 int, nota2 int, nota3 int, nota4 int)
    RETURNS int
    COMMENT 'Función mínimo'
  BEGIN
    DECLARE resultado int DEFAULT 0;
    SET resultado = LEAST(nota1,nota2,nota3,nota4); 
    RETURN resultado;
  END //
DELIMITER ;

SELECT ej15(60,40,50,35);

/* Ej16 - Función máximo */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej16(nota1 int, nota2 int, nota3 int, nota4 int)
    RETURNS int
    COMMENT 'Función máximo'
  BEGIN
    DECLARE resultado int DEFAULT 0;
    SET resultado = GREATEST(nota1,nota2,nota3,nota4); 
    RETURN resultado;
  END //
DELIMITER ;

SELECT ej16(60,40,50,35);

/* Ej17 - Función moda */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej17(nota1 int, nota2 int, nota3 int, nota4 int)
    RETURNS int
    COMMENT 'Función moda'
  BEGIN
    DECLARE resultado int DEFAULT 0;
    DECLARE maximo int DEFAULT 0;

    CREATE OR REPLACE TEMPORARY TABLE ej17(
      id int AUTO_INCREMENT,
      notas int,
      PRIMARY KEY(id)
      );
    
    INSERT INTO ej17 (notas)
      VALUES  (nota1),
              (nota2),
              (nota3),
              (nota4);

    SELECT 
      MAX(c1.numrepetidos) INTO maximo
      FROM 
        (
          SELECT 
            e.notas,COUNT(*) numrepetidos 
            FROM 
              ej17 e 
            GROUP BY 
              e.notas
        ) c1;

    SELECT 
      AVG(c1.notas) INTO resultado
      FROM 
        (
          SELECT 
            e.notas,COUNT(*) numrepetidos 
            FROM 
              ej17 e 
            GROUP BY 
              e.notas
        ) c1
      WHERE
        numrepetidos=maximo;

    RETURN resultado;
  END //
DELIMITER ;

SELECT ej17(60,40,40,35);

/* Ej18 - Procedimiento almacenado alumnos */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej18(id1 int,id2 int)
    BEGIN     
      
      UPDATE 
        alumnos a 
          SET a.media = ej14(a.nota1,a.nota2,a.nota3,a.nota4),
              a.min = ej15(a.nota1,a.nota2,a.nota3,a.nota4),
              a.max = ej16(a.nota1,a.nota2,a.nota3,a.nota4),
              a.moda = ej17(a.nota1,a.nota2,a.nota3,a.nota4)
          WHERE a.id BETWEEN IF(id1<id2,id1,id2) AND IF(id2>id1,id2,id1);    
      
      UPDATE
        grupos g
          SET g.media = (SELECT AVG(a.media) FROM alumnos a WHERE a.grupo=1),
              g.max = (SELECT max(a.max) FROM alumnos a WHERE a.grupo=1),
              g.min = (SELECT min(a.min) FROM alumnos a WHERE a.grupo=1)
          WHERE id=1;

      
      UPDATE
        grupos g
          SET g.media = (SELECT AVG(a.media) FROM alumnos a WHERE a.grupo=2),
              g.max = (SELECT max(a.max) FROM alumnos a WHERE a.grupo=2),
              g.min = (SELECT min(a.min) FROM alumnos a WHERE a.grupo=2)
          WHERE id=2;

         
    END //
DELIMITER ;

CALL ej18(10,20);

SELECT * FROM grupos;
SELECT * FROM alumnos a;

/* Ej19 - Procedimiento almacenado conversión */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej19(id int)
    COMMENT 'Procedimiento almacenado conversión'
    BEGIN
      UPDATE conversion c
        set c.m = c.cm / 100,
            c.km = c.cm / 100000,
            c.pulgadas = c.cm / 2.54
        WHERE id = c.id AND c.cm IS NOT NULL;
      
      UPDATE conversion c
        set c.cm = c.m * 100,
            c.km = c.m / 1000,
            c.pulgadas = c.m * 39.37
        WHERE id=c.id AND c.m IS NOT NULL;

      UPDATE conversion c
        set c.cm = c.km / 100000,
            c.m = c.km / 1000,
            c.pulgadas = c.km * 39370.079
        WHERE id=c.id AND c.km IS NOT NULL;
      
      UPDATE conversion c
        set c.cm = c.pulgadas * 2.54,
            c.m = c.pulgadas / 39.37,
            c.km = c.pulgadas / 39370.079                        
        WHERE id=c.id AND c.pulgadas IS NOT NULL;

    END //
DELIMITER ;

CALL ej19(2);

SELECT * FROM conversion c;